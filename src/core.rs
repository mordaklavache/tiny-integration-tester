use crate::dyn_lib;
use crate::json;
use crate::testable::Testable;
use crate::CommandArguments;

use colored::Colorize;
use libloading::Symbol;
#[cfg(unix)]
use nix::unistd::{close, pipe, read};

use std::io::{BufWriter, Write};
#[cfg(unix)]
use std::os::fd::{FromRawFd, RawFd};
#[cfg(unix)]
use std::os::unix::process::ExitStatusExt;
use std::process::{Command, ExitCode, Output, Stdio};
#[cfg(unix)]
use std::sync::mpsc;
#[cfg(unix)]
use std::thread;

#[cfg(unix)]
#[derive(Copy, Clone, Debug)]
struct NixPipes {
    stdout: (RawFd, RawFd),
    stderr: (RawFd, RawFd),
}

/// Here's how it is represented on the RUST side.
#[derive(Debug)]
struct Test {
    context: Box<dyn Testable>,
    command: Command,
    #[cfg(unix)]
    pipes: Option<NixPipes>,
}

/// Test evaluation.
fn evaluate(output: &Output, test: &mut Test) -> Result<String, String> {
    #[cfg(unix)]
    let out_status = ExitStatusExt::into_raw(output.status);
    #[cfg(windows)]
    let out_status = output.status.code().unwrap();
    if !test
        .context
        .output(out_status, &output.stdout, &output.stderr)
    {
        Err("FAIL!".color("red").on_color("white").to_string())
    } else {
        Ok("OK!".color("green").on_truecolor(30, 30, 30).to_string())
    }
}

/// Final summary
fn summary(success: usize, nb_test: usize) {
    let summary = format!("result: {} / {}", success, nb_test);
    println!("{}", summary.color("red").on_color("white"));
}

mod built_info {
    include!(concat!(env!("OUT_DIR"), "/built.rs"));
}

pub fn make_tests(args: CommandArguments) -> ExitCode {
    let terminaison_requested = "Terminaison requested".color("red").on_color("white");

    let init_test = |context: Box<dyn Testable>| -> Test {
        let mut command = Command::new(&context.command());
        if context.env_clear() {
            command.env_clear();
        }
        command.stdin(Stdio::piped());
        command.args(context.args().iter());
        command.envs(context.envs().into_iter());
        if let Some(path) = context.dir() {
            command.current_dir(path);
        }

        #[cfg(unix)]
        let pipes = if match (args.default_show_output, context.show_output()) {
            (true, None | Some(true)) => true,
            (true, Some(false)) => false,
            (false, None | Some(false)) => false,
            (false, Some(true)) => true,
        } {
            let (stdout_read_end, stdout_write_end) = pipe().unwrap();
            let (stderr_read_end, stderr_write_end) = pipe().unwrap();
            command.stdout(unsafe { Stdio::from_raw_fd(stdout_write_end) });
            command.stderr(unsafe { Stdio::from_raw_fd(stderr_write_end) });
            Some(NixPipes {
                stdout: (stdout_read_end, stdout_write_end),
                stderr: (stderr_read_end, stderr_write_end),
            })
        } else {
            command.stdout(Stdio::piped());
            command.stderr(Stdio::piped());
            None
        };
        #[cfg(windows)]
        {
            command.stdout(Stdio::piped());
            command.stderr(Stdio::piped());
        }
        Test {
            context,
            command,
            #[cfg(unix)]
            pipes,
        }
    };

    let filename = match args.filename {
        Some(name) => name,
        None => json::DEFAULT_JSON_FILENAME.into(),
    };

    let tests = match args.json {
        // Open the JSON file and deserialize it
        true => match json::import(filename) {
            Ok(tests) => tests
                .into_iter()
                .map(|context| init_test(context))
                .collect::<Vec<Test>>(),
            Err(e) => {
                eprintln!("{}", e);
                return ExitCode::FAILURE;
            }
        },
        // Build dynamic rust library and import test from it
        false => {
            log::info!(
                "This program was built with toolchain : {}.",
                built_info::RUSTC_VERSION
            );
            match dyn_lib::import(args.toolchain, args.filename) {
                Ok(lib) => {
                    let tests = unsafe {
                        let func: Symbol<
                            unsafe fn(
                                &[&str],
                            )
                                -> Result<Vec<Box<dyn Testable>>, Box<dyn std::error::Error>>,
                        > = match lib.get(b"TESTS") {
                            Ok(func) => {
                                log::info!("Entry point linked!");
                                func
                            }
                            Err(_) => {
                                log::error!("No entry point was founded into the library!");
                                println!("Please export a function with the following signature:");
                                println!(
                                    "#[no_mangle] pub fn TESTS(args: &[&str]) -> Result<Vec<Box<dyn Testable>>, Box<dyn std::error::Error>>"
                                );
                                return ExitCode::FAILURE;
                            }
                        };
                        (*func)(args.arguments)
                    };
                    std::mem::forget(lib); // Keep dyn lib into memory
                    match tests {
                        Ok(tests) => tests
                            .into_iter()
                            .map(|context| init_test(context))
                            .collect::<Vec<Test>>(),
                        Err(e) => {
                            eprintln!("An Error has occured from entry point! err = {}", e);
                            return ExitCode::FAILURE;
                        }
                    }
                }
                Err(e) => {
                    eprintln!("{}", e);
                    return ExitCode::FAILURE;
                }
            }
        }
    };

    // Start all the tests
    let mut success = 0;
    let nb_test = tests
        .iter()
        .fold(0, |sum, test| sum + if test.context.test() { 1 } else { 0 });
    for mut test in tests {
        println!(
            "\n{} {}\nExecuting command: {:?} with args: {:?}",
            "Test".color("green").on_truecolor(30, 30, 30),
            test.context.name(),
            test.command.get_program(),
            test.command
                .get_args()
                .map(|v| v.to_os_string().into_string().unwrap())
                .collect::<Vec<String>>()
        );
        match test.command.spawn() {
            Ok(mut child) => {
                // This thread manages the timeout
                #[cfg(unix)]
                let killer_ctx = {
                    let timeout = test.context.timeout().or(args.default_timeout);
                    if let Some(duration) = timeout {
                        let (sender, receiver) = mpsc::channel();
                        let pid = child.id();
                        let thread = thread::spawn(move || {
                            if let Err(_) = receiver.recv_timeout(duration) {
                                unsafe {
                                    libc::kill(pid as i32, libc::SIGTERM);
                                }
                                let err_msg = format!("Killed by OS signal SIGTERM");
                                eprintln!("{}", err_msg.color("red").on_color("white"));
                            };
                        });
                        Some((thread, sender))
                    } else {
                        None
                    }
                };

                // Write data into stdin
                if let Some(stdin) = &test.context.stdin() {
                    let mut outstdin = &mut child.stdin.as_mut().unwrap();
                    let mut writer = BufWriter::new(&mut outstdin);
                    writer.write_all(stdin.as_bytes()).unwrap();
                }
                // These threads are used to display the stdout and stderr of the commands
                #[cfg(unix)]
                let threads = if let Some(pipes) = test.pipes {
                    fn output_thread(pipe: RawFd) -> thread::JoinHandle<std::io::Result<Vec<u8>>> {
                        thread::spawn(move || -> std::io::Result<Vec<u8>> {
                            let mut buf = vec![0; 1024];
                            let mut output = Vec::new();
                            loop {
                                match read(pipe, &mut buf) {
                                    Ok(n) if n > 0 => {
                                        output.extend_from_slice(&buf[..n]);
                                        std::io::stdout().write_all(&buf[..n])?;
                                    }
                                    Ok(_) => break,
                                    Err(e) => panic!("Error reading from pipe! err = {}", e),
                                }
                            }
                            Ok(output)
                        })
                    }
                    let stdout_thread = output_thread(pipes.stdout.0);
                    let stderr_thread = output_thread(pipes.stderr.0);
                    Some((stdout_thread, stderr_thread))
                } else {
                    None
                };

                // Wait for end of commands
                #[cfg(unix)]
                let mut output = child.wait_with_output().expect("Unexpected error!");
                #[cfg(windows)]
                let output = child.wait_with_output().expect("Unexpected error!");

                // Terminate the thread that is handling the timeout
                #[cfg(unix)]
                if let Some((thread, sender)) = killer_ctx {
                    let _r = sender.send(());
                    thread.join().expect("The thread has panicked");
                }

                // In case of showing output, take all the data into Output
                #[cfg(unix)]
                if let Some((stdout_thread, stderr_thread)) = threads {
                    close(test.pipes.unwrap().stdout.1).unwrap();
                    close(test.pipes.unwrap().stderr.1).unwrap();
                    output.stdout = stdout_thread.join().unwrap().unwrap();
                    output.stderr = stderr_thread.join().unwrap().unwrap();
                }

                // Evaluation sequence
                if test.context.test() {
                    match evaluate(&output, &mut test) {
                        Ok(s) => {
                            println!("{s}");
                            success += 1;
                        }
                        Err(s) => {
                            eprintln!("{s}");
                            if test.context.terminate() {
                                summary(success, nb_test);
                                eprintln!("{}", &terminaison_requested);
                                return ExitCode::FAILURE;
                            }
                        }
                    }
                }
            }
            Err(error) => {
                let err_msg = format!("cannot execute command! err : {}", error);
                eprintln!("{}", err_msg.color("red").on_color("white"));
                if test.context.terminate() {
                    summary(success, nb_test);
                    eprintln!("{}", &terminaison_requested);
                    return ExitCode::FAILURE;
                }
            }
        }
    }
    summary(success, nb_test);
    match success == nb_test {
        true => ExitCode::SUCCESS,
        false => ExitCode::FAILURE,
    }
}
