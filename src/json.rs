use std::ffi::OsStr;
use std::fs::{File, OpenOptions};
use std::io::{ErrorKind, Read, Seek, SeekFrom, Write};
use std::path::Path;

#[cfg(unix)]
use std::time::Duration;

use serde::Deserialize;

use crate::testable::Testable;

/// Default json file for new rust project.
const BOILERPLATE_JSON: &str = "[
    {
        \"command\" : \"cargo\",
        \"name\" : \"build program\",
        \"args\" : [\"build\"],
        \"status\" : \"0x0\",
        \"terminate\" : true,
        \"show_output\" : true
    },
    {
        \"command\" : \"cargo\",
        \"name\" : \"make tests\",
        \"args\" : [\"test\"],
        \"status\" : \"0x0\",
        \"terminate\" : true,
        \"show_output\" : true
    }
]
";
/// The default Json filename is simply `test.json`.
pub const DEFAULT_JSON_FILENAME: &str = "test.json";

/// Here's how it is represented on the JSON side.
#[allow(dead_code)]
#[derive(Debug, Deserialize)]
pub struct JSONTest {
    command: String,
    name: Option<String>,
    args: Option<Vec<String>>,
    dir: Option<String>,
    stdin: Option<String>,
    status: Option<String>,
    stdout: Option<String>,
    stderr: Option<String>,
    timeout: Option<u64>,
    env_clear: Option<bool>,
    envs: Option<Vec<(String, String)>>,
    test: Option<bool>,
    terminate: Option<bool>,
    show_output: Option<bool>,
}

impl Testable for JSONTest {
    fn command(&self) -> &OsStr {
        self.command.as_ref()
    }

    fn name(&self) -> &str {
        self.name
            .as_ref()
            .map(|s| s.as_str())
            .unwrap_or("json-test")
    }
    fn args(&self) -> Vec<&OsStr> {
        match &self.args {
            Some(args) => args.iter().map(|e| e.as_ref()).collect::<Vec<&OsStr>>(),
            None => Vec::new(),
        }
    }
    fn env_clear(&self) -> bool {
        self.env_clear.unwrap_or(false)
    }
    fn envs(&self) -> Vec<(&OsStr, &OsStr)> {
        match &self.envs {
            Some(args) => args
                .iter()
                .map(|(k, v)| (k.as_ref(), v.as_ref()))
                .collect::<Vec<(&OsStr, &OsStr)>>(),
            None => Vec::new(),
        }
    }
    fn dir(&self) -> Option<&Path> {
        self.dir.as_ref().map(|dir| dir.as_ref())
    }

    fn stdin(&self) -> Option<&str> {
        self.stdin.as_deref()
    }

    fn test(&self) -> bool {
        self.test.unwrap_or(true)
    }
    fn terminate(&self) -> bool {
        self.terminate.unwrap_or(false)
    }
    #[cfg(unix)]
    fn show_output(&self) -> Option<bool> {
        self.show_output
    }
    #[cfg(unix)]
    fn timeout(&self) -> Option<std::time::Duration> {
        self.timeout.map(|t| Duration::from_millis(t))
    }
    fn output(&mut self, status: i32, stdout: &[u8], stderr: &[u8]) -> bool {
        (match &self.status {
            Some(str_status) => i32::from_str_radix(&str_status[2..], 16).unwrap() == status,
            None => true,
        }) && match &self.stdout {
            Some(ref_stdout) => stdout == ref_stdout.as_bytes(),
            None => true,
        } && match &self.stderr {
            Some(ref_stderr) => stderr == ref_stderr.as_bytes(),
            None => true,
        }
    }
}

/// Simply open and read the json file.
fn open_json_file(filename: &str) -> std::io::Result<String> {
    let mut f = match File::open(filename) {
        Ok(f) => f,
        Err(e) => {
            if e.kind() == ErrorKind::NotFound && Path::new("Cargo.toml").exists() {
                let mut file = OpenOptions::new()
                    .read(true)
                    .write(true)
                    .create(true)
                    .open(filename)?;
                file.write_all(BOILERPLATE_JSON.as_bytes())?;
                file.seek(SeekFrom::Start(0))?;
                file
            } else {
                eprintln!("cannot process");
                Err(e)?
            }
        }
    };
    let mut data = String::new();
    f.read_to_string(&mut data)?;
    Ok(data)
}

pub fn import(filename: &str) -> std::io::Result<Vec<Box<JSONTest>>> {
    let content = open_json_file(&filename)?;

    // Generation of a Vec<Test> instance from JSON directives
    Ok(serde_json::from_str::<Vec<JSONTest>>(&content)
        .expect("cannot parse json file")
        .into_iter()
        .map(|v| Box::new(v))
        .collect::<Vec<Box<JSONTest>>>())
}
