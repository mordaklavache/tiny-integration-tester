use std::fs;
use std::fs::OpenOptions;
use std::io::Write;
use std::path::Path;
use std::process::{exit, Command, Stdio};

use libloading::Library;
use toml::Value;

const DYN_LIBRARY_NAME: &str = "integration-tests";
const TESTABLE: &str = include_str!("testable.rs");
const BOILERPLATE_STRUCTURE: &str = include_str!("boilerplate_structure.rs");
const LIB_RS: &str = "mod boilerplate_structure;
mod testable;

pub use boilerplate_structure::{Attribute, IntTest};
pub use testable::Testable;

use std::ffi::OsStr;

/// Test for `cargo build` command.
#[derive(Debug)]
pub struct CargoBuilder;

// YOU CAN IMPLEMENT THE `Testable` TRAIT TO CREATE A NEW TEST ...
#[cfg_attr(rustfmt, rustfmt_skip)]
impl Testable for CargoBuilder {
    fn command(&self) -> &OsStr { \"cargo\".as_ref() }
    fn name(&self) -> &str { \"Build program\" }
    fn args(&self) -> Vec<&OsStr> { vec![\"build\".as_ref()] }
    fn terminate(&self) -> bool { true }
    fn show_output(&self) -> Option<bool> { Some(true) }
    fn output(&mut self, status: i32, _out: &[u8], _err: &[u8]) -> bool { status == 0 }
}

/// This main function will be exported.
#[no_mangle]
#[allow(non_snake_case)]
pub fn TESTS(_args: &[&str]) -> Result<Vec<Box<dyn Testable>>, Box<dyn std::error::Error>> {
    // ... OR YOU CAN USE THE `IntTest` STRUCTURE, WHICH IS A VERY HANDY BOILERPLATE.
    let cargo_test = Box::new(
        IntTest::builder(\"cargo\")
            .args([\"test\"])
            .output(|status: i32, _out: &[u8], _err: &[u8]| status == 0)
            .set_attribute(Attribute {
                name: \"Basic cargo test\".to_string(),
                terminate: true,
                show_output: Some(true),
                ..Default::default()
            }),
    );

    Ok(vec![Box::new(CargoBuilder {}), cargo_test])
}";

const GITIGNORE: &str = "target/
";

fn write_text_file(pathname: &str, content: &str) -> Result<(), Box<dyn std::error::Error>> {
    OpenOptions::new()
        .create(true)
        .write(true)
        .truncate(true)
        .open(pathname)?
        .write_all(content.as_bytes())?;
    Ok(())
}

fn modify_cargo_toml_content(content: &str) -> String {
    let mut value: Value = content
        .parse::<Value>()
        .expect("Could not parse the Cargo.toml!");

    let mut lib_section = toml::value::Table::new();
    lib_section.insert(
        "crate-type".to_string(),
        Value::Array(vec![Value::String("dylib".to_string())]),
    );

    fn insert(tree: &mut Value, section: String, value: Value) {
        tree.as_table_mut().unwrap().insert(section, value);
    }
    let refer = &mut value;
    insert(refer, "lib".into(), Value::Table(lib_section));
    insert(refer, "workspace".into(), Value::Table(toml::Table::new()));

    toml::to_string_pretty(&value).unwrap()
}

fn exec_command(
    command: &str,
    dirname: Option<&str>,
    args: &[&str],
) -> Result<(), Box<dyn std::error::Error>> {
    let mut command = Command::new(command);
    command
        .args(args)
        .stdout(Stdio::inherit())
        .stderr(Stdio::inherit());
    if let Some(dirname) = dirname {
        command.current_dir(dirname);
    }
    let mut child = command.spawn()?;
    // unstable API : pub fn exit_ok(status: &ExitStatus) -> Result<(), ExitStatusError>
    match child.wait()?.success() {
        true => Ok(()),
        false => Err(Box::new(std::io::Error::new(
            std::io::ErrorKind::Other,
            "Command failure",
        ))),
    }
}

pub fn import(
    toolchain: Option<&str>,
    filename: Option<&str>,
) -> Result<Library, Box<dyn std::error::Error>> {
    let (package_name, lib_dir_name) = match std::fs::read_to_string("Cargo.toml") {
        Ok(content) => {
            log::info!("A Cargo.toml was found!");
            let value = content.parse::<Value>().map_err(|e| {
                log::error!("Could not parse the Cargo.toml, maybe invalid format?");
                e
            })?;
            let package_name = value
                .get("package")
                .and_then(|package| package.get("name"))
                .and_then(Value::as_str)
                .unwrap_or_else(|| {
                    log::error!("Cant find package name!");
                    exit(-1);
                });
            (
                package_name.to_string(),
                match filename {
                    Some(filename) => filename.to_string(),
                    None => format!("{}-{}", DYN_LIBRARY_NAME, package_name),
                },
            )
        }
        Err(_e) => {
            log::info!("No Cargo.toml file found or cannot read it!");
            unimplemented!();
        }
    };

    let arg_toolchain = toolchain.map(|toolchain| format!("+{}", toolchain));

    if !Path::new(&lib_dir_name).exists() {
        log::info!("Writing of integration tests crate.");

        let mut args = Vec::new();
        if let Some(toolchain_argument) = &arg_toolchain {
            args.push(toolchain_argument.as_str());
        }
        args.extend(["new", "--lib", &lib_dir_name]);
        exec_command("cargo", None, &args)?;

        let pathname = format!("{}/Cargo.toml", lib_dir_name);
        let content = fs::read_to_string(&pathname).map_err(|e| {
            log::error!("Could not read the new Cargo.toml");
            e
        })?;
        let new_content = modify_cargo_toml_content(&content);
        write_text_file(&pathname, &new_content)?;

        let pathname = format!("{}/.gitignore", lib_dir_name);
        write_text_file(&pathname, GITIGNORE)?;

        let pathname = format!("./{}/src/lib.rs", lib_dir_name);
        let content = format!(
            "//! This crate contains integration tests for {}.\n\n{}",
            package_name, LIB_RS
        );
        write_text_file(&pathname, &content)?;

        let pathname = format!("./{}/src/testable.rs", lib_dir_name);
        write_text_file(&pathname, TESTABLE)?;

        let pathname = format!("./{}/src/boilerplate_structure.rs", lib_dir_name);
        write_text_file(&pathname, BOILERPLATE_STRUCTURE)?;
    }

    match toolchain {
        Some(toolchain) => log::info!("Compiling dyn lib with toolchain: {}", toolchain),
        None => {
            log::info!("Compiling dyn lib with toolchain:");
            exec_command("rustup", Some(&lib_dir_name), &["show", "active-toolchain"])?;
        }
    }

    let mut args = Vec::new();
    if let Some(toolchain_argument) = &arg_toolchain {
        args.push(toolchain_argument.as_str());
    }
    args.extend(["build", "--release"]);
    exec_command("cargo", Some(&lib_dir_name), &args).map_err(|err| {
        log::error!("Cannot compile integrations test dyn lib! err = {}", err);
        err
    })?;

    log::info!("Linking library.");
    let lib_file_name = format!(
        "{}/target/release/lib{}.so",
        lib_dir_name,
        lib_dir_name.replace("-", "_")
    );
    unsafe { Ok(Library::new(lib_file_name)?) }
}
